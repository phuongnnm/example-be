// postgres, mysql, mariadb

const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('example1', 'postgres', '123dsa', {
    host: 'localhost',
    dialect: 'postgres'
});

sequelize.authenticate()
    .then(() => {
        console.log("Connected to DB")
    })
    .catch(err => {
        console.log(`Error occured: ${err.message}`)
    })

sequelize.sync().then(() => {
    console.log('synced');
})

module.exports = sequelize