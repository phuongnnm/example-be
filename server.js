//host: 3000

let express = require('express');
let bodyparser = require('body-parser')
let route = require('./route')
let app = express()

app.use(bodyparser.json())
//GET request => '/'
app.get('/', (req, res) => {
    res.send({ message: "Phuong's webserver" })
})

app.use('/api', route)

app.listen(3000, () => {
    console.log("Server is Running")
})

