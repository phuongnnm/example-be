const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../connection')

const User = sequelize.define('User', {
  // Model attributes are defined here
  id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  timestamps: false // updatedAt & createdAt
});

module.exports = User
