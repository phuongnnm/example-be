let express = require('express');
let route = express.Router();
let userModel = require('./model/user');

// GET request
route.get('/user', (req, res) => {
    userModel.findAll().then((data) => {
        res.json(data)
    })
})

route.get('/user/:id', (req, res) => {
    userModel.findAll({
        where: {
            id: req.params.id
        }
    })
    .then((data) => {
        res.json(data)
    })
})
//POST request => Create (CRUD)
route.post('/user', async (req, res) => {
    console.log(req.body)
    if (req.body.user) {
        const user = req.body.user
        await userModel.create({
            firstName: user.firstName,
            lastName: user.lastName
        }).then((data) => {
            res.json(data)
        })
    }
})

module.exports = route
